           jQuery(document).ready(function($) {
    var $owl = $('#gallery');
    var $owlBtns = $('#btns-list');

    
      
      $owl.owlCarousel({
        center: true,
        nav:false,
        loop: true,
        items: 3,
      margin:10,
        navText: ["<i class='fa arrow-circle-left'><</i>","<i class='fa arrow-right'>></i>"],
        responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:3
          },
          745:{
              items:2
          }
       }
      });
      
      $owlBtns.owlCarousel({
        center: true,
        nav:false,
        loop: true,
        items: 4,
      margin:10,
        navText: ["<i class='fa arrow-circle-left'><</i>","<i class='fa arrow-right'>></i>"],
        responsive:{
          0:{
              items:1
          },
          600:{
              items:3
          },
          1000:{
              items:4
          }
       }
      });
   
          });